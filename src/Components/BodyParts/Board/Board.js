import React from 'react';
import classes from './Board.css';
import Aux from '../../../hoc/Aux';

const Board = (props) => {
    
    return (
        <Aux>
        <div className={classes.Board}>
        <div className={classes.regDropdown}>
            <select>
                <option>css</option>
                <option selected>Registration Information</option>
                <option>sass</option>
                <option>less</option>
            </select>
        </div>
        <p className={classes.stepsTitle} style={{marginTop:'20px'}}>&larr;  Dashboard</p>
            <div className={classes.container}>
                <ul className={classes.progressbar}>
                    <li className={classes.active}><p>Step 1</p><p className={classes.stepInfo}> Personal Information </p></li>
                    <li><p>Step 2</p><p className={classes.stepInfo}>Perents details</p></li>
                    <li><p>Step 3</p><p className={classes.stepInfo}>Educational details </p></li>
                    <li><p>Step 4</p><p className={classes.stepInfo}>Payment details</p></li>
                </ul>
            </div>
            <div className={classes.BoardHelpLine}> <p className={classes.helpLine}>Helpline number</p><p className={classes.helpLineNumber}><big style={{marginTop:"30px"}}>020-2445-4374</big></p></div>
        </div>
        </Aux>
    )
}

export default Board;
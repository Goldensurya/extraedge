import React from 'react';
import classes from './TimeLine.css';
//import SelectRequire from '../../InputDatas/SelectRequire/SelectRequire';

import InputTypeRequire from '../../InputDatas/InputTypeRequire/InputTypeRequire';
import Helper from './Helper';
const TimeLine = (props) => {
    return (
        <div className={classes.TimeLine}>
            <p className={classes.stepTitle}>Step1: Personal Information</p>
            <b style={{paddingTop:'20px',paddingBottom:'20px',color:'#202020',fontWeight:'400'}}>Personal details</b>
            <Helper 
            infoleft={"Gender"}
            getoneleft={"Select gender"}
            gettwoleft={"Male"}
            getthreeleft={"Female"}
            getfourleft={"Other"}
            info2={"Bood group"}
            getone={"A+"}
            gettwo={"B+"}
            getthree={"C+"}/>
            <div className={classes.suprate} style={{marginTop:'20px'}}>    
                <InputTypeRequire info={"Place of birth"} placeholder={"Enter place of birth"}/>
            </div>  
            <Helper 
                need={"text"} 
                info1={"Nationality"} 
                info2={"Country of citizenship"}
                getone={"India"}
                gettwo={"pakistan"}
                getthree={"USA"}
                placeholder={"Enter Nationality"}
                
                />
            <Helper need={"text"} 
                    info1={"Domicile"} 
                    info2={"Category"}
                    getone={"North"}
                    gettwo={"West"}
                    getthree={"East"}
                    placeholder={"Domicile"}/>

            <Helper need={"TextRight"} 
                    info1={"Nationality"} 
                    info2={"Category"}
                    getone={"North"}
                    gettwo={"West"}
                    getthree={"East"}/>
            <br/>
            <br/>
            <b style={{paddingTop:'20px',paddingBottom:'20px',color:'#202020',fontWeight:'400'}}>Passport details</b>
            <Helper need={"BothSideText"} 
                    info1={"Passport Number"} 
                    placeholder={"Add Passport Number"}
                    info2={"Place of issue"}
                    getone={"North"}
                    gettwo={"West"}
                    getthree={"East"}
                    placeholder={"Add place of issue"}/>

            <Helper need={"BothSideText"} 
                    info1={"Date of issue"}
                    placeholder={"Enter date of issue"}
                    info2={"Expiry date"}
                    getone={"North"}
                    gettwo={"West"}
                    getthree={"East"}
                    placeholder={"Enter expiry date"}/>
<br/>
            <br/>
            <b style={{paddingTop:'20px',paddingBottom:'20px',color:'#202020',fontWeight:'400'}}>Present address</b>
            <Helper need={"BothSideText"} 
                    info1={"Address line 1"}
                    placeholder={"Enter address line 1"}
                    info2={"Address line 2"}
                    getone={"North"}
                    gettwo={"West"}
                    getthree={"East"}
                    placeholder={"Enter address line 2"}/>  
            <Helper 
                need={"text"} 
                info1={"Pincode"} 
                placeholder={"Enter pin code"}
                info2={"Country"}
                getone={"Select country"}
                gettwo={"India"}
                getthree={"pakistan"}
                getfour={"USA"}
                
                />
            <Helper need={"text"} 
                    info1={"State"} 
                    placeholder={"Select state"}
                    info2={"City"}
                    getone={"Select city"}
                    gettwo={"West"}
                    getthree={"East"}/>
            <div className={classes.suprate} style={{marginTop:'20px'}}>    
                <InputTypeRequire info={"Alternate phone number"} placeholder={"Add phone number"}/>
            </div>
            <div className={classes.addressCheckbox}>
                <input type="checkbox" style={{marginTop:"35px"}}/> Permanent address same as present address
            </div>
            <div className={classes.saveContinue}>
                <input type="button" className={classes.button} value="Save & Continue"/>
            </div>
            <div className={classes.BoardHelpLine}> <p className={classes.helpLine}>Helpline number</p><p className={classes.helpLineNumber}><big style={{marginTop:"30px"}}>020-2445-4374</big></p></div>
        </div>
        
    )
}

export default TimeLine;
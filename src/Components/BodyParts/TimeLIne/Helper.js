import React from 'react';
import SelectRequire from '../../InputDatas/SelectRequire/SelectRequire';
import classes from '../TimeLIne/TimeLine.css';
import InputTypeRequire from '../../InputDatas/InputTypeRequire/InputTypeRequire';

const Helper  = (props) => {

    if(props.need === 'text'){
        return (
            <div className={classes.flex_container}>
                 <div className={classes.suprate}>    
                 <InputTypeRequire info={props.info1} placeholder={props.placeholder}/>
                 </div>
                 <div className={classes.suprate}>
                <SelectRequire 
                    className={classes.selectStyle} 
                    info={props.info2} 
                    getone={props.getone}
                    gettwo={props.gettwo}
                    getthree={props.getthree}
                    />                    
                 </div>
            
            </div>
        )
    }else if(props.need === 'TextRight')
    {
        return (
            <div className={classes.flex_container}>
                 <div className={classes.suprate}>    
                 <SelectRequire 
                    className={classes.selectStyle} 
                    info={props.info2} 
                    getone={props.getone}
                    gettwo={props.gettwo}
                    getthree={props.getthree}
                    /> 
                 
                 </div>
                 <div className={classes.suprate}>
                 <InputTypeRequire info={props.info1} placeholder={props.placeholder}/>                 
                 </div>
            
            </div>
        ) 
    }
    else if(props.need === 'BothSideText')
    {
        return (<div className={classes.flex_container}>
                 
                 <div className={classes.suprate}>
                 <InputTypeRequire info={props.info1} placeholder={props.placeholder}/>                 
                 </div>
                 <div className={classes.suprate}>
                 <InputTypeRequire info={props.info2} placeholder={props.placeholder}/>                 
                 </div>
            
        </div>);
    }
    else
    {
        return (
            <div className={classes.flex_container}>
                <div className={classes.suprate}>    
                 <SelectRequire className={classes.selectStyle} 
                 info={props.infoleft} 
                 getone={props.getoneleft}
                 gettwo={props.gettwoleft}
                 getthree={props.getthreeleft} />
                 </div>

                 <div className={classes.suprate}>
                <SelectRequire className={classes.selectStyle}  
                info={props.info2} 
                getone={props.getone}
                gettwo={props.gettwo}
                getthree={props.getthree}/>                    
                 </div>
            
             </div>
        );
    }

}

export default Helper;
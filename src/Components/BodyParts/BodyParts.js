import React from 'react';
import Aux from '../../hoc/Aux';
import Board from './Board/Board';
import TimeLine from './TimeLIne/TimeLine';
import classes from './BodyParts.css';


const BodyParts = (props) => {
    return (
        <Aux>
            <div className={classes.BodyParts}>
                <Board  />
                <TimeLine />
            </div>
        </Aux>
    );
}

export default BodyParts;
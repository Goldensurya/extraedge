import React from 'react'; 
import Aux from '../../hoc/Aux';
import NavigationItmes from '../NavigationItems/NavigationsItems';
import BodyParts from '../BodyParts/BodyParts';
import Footer from '../Footer/Footer';

const layout = (props) => (
    <Aux>
        <header>
            <NavigationItmes></NavigationItmes>
        </header>
        
        <main>
            <BodyParts />
        </main>
        <Footer />
    </Aux>
);

export default layout;

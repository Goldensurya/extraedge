import React from 'react';
import classes from './NavigationsItems.css'; 
import Aux from '../../hoc/Aux';
import ProfileLogo from '../../Assets/images/avatar.png'
const navigationItems = (props) => {
 
    return (
        <Aux>
            <div className={classes.header}>
                <a href="#default" className={classes.logo}>
                    <span 
                        className={classes.Lorem}>Lorem</span>&nbsp; 
                        <span className={classes.University}>University</span>
                </a>
                <p className={classes.OAF}> Online Application Form - 2019</p>
            <div className={classes.headerRight}> 
                <span className={classes.userIcon}><i className="glyphicon glyphicon-user"></i></span>
                
            <select className={classes.ProfileHolder}>
                <option selected>Donald Young</option>
                <option >Upload photo</option>
                <option>Customer Succes Story</option>
                <option>PotentialEDGE</option>
            </select>
            </div>
        </div>
</Aux>
)
}

export default navigationItems;
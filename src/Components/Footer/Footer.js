import React from 'react';
import classes from './Footer.css';

const Footer = (props) => {
    return (
        <footer className={classes.footerWrapper}>
        <div>
            <div className={classes.termCondition}>Terms and Conditions 
                <span style={{color:'gray',padding: "3px"}}>for filling Application Form
                </span>
            </div>
            <div className={classes.poweredBy}>Powered by: 
                <span style={{color:'orange'}}>extraaedge
                </span>
            </div>
        </div>
        </footer>
    );
}

export default Footer;
import React from 'react';
import classes from './InputTypeRequire';

const InputTypeRequire = (props) => {


    return (
        <div className={classes.desing}>
            <label>{props.info}</label><br/>
            <input className={classes.textStyle} type="text" id="fname" name="firstname" placeholder={props.placeholder} required/>
        </div>
    );
}
    

export default InputTypeRequire;

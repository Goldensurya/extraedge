import React from 'react';
import classes from './SelectRequire';

const InputTypeRequire = (props) => {


    return (
        <div className={classes.styleDiv}>
            <label>{props.info}</label><br/>
            <select className={classes.selectStyle} id="country" name="country">
            <option value="1">{props.getone}</option>
            <option value="2">{props.gettwo}</option>
            <option value="3">{props.getthree}</option>
            </select>
        </div>
    );
}
    

export default InputTypeRequire;
